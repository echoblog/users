package handlers

// var (
// 	expireTime = time.Hour * 24 * 30
// )

// // Tokens handler
// type Tokens struct {
// 	M *models.ModelHandler
// }

// // New user token
// func (t *Tokens) New(ctx context.Context, in *tokensproto.EchoblogTokenNewREQ, out *tokensproto.EchoblogTokenNewRES) error {
// 	var et time.Time
// 	if in.Expiretime <= 0 {
// 		et = time.Now().Add(expireTime)
// 	} else {
// 		et = time.Unix(in.Expiretime, 0)
// 	}
// 	if in.Uid <= 0 {
// 		out.Errcode = 1
// 		return nil
// 	}
// 	tStr := ksuid.New().String()
// 	err := t.M.InsertToken(&models.Tokens{
// 		UID:        int(in.Uid),
// 		Token:      tStr,
// 		ExpireTime: et,
// 	})
// 	if err != nil {
// 		out.Errcode = 5
// 		return nil
// 	}
// 	out.Token = tStr
// 	out.Errcode = 0
// 	return nil
// }

// // Get token details
// func (t *Tokens) Get(ctx context.Context, in *tokensproto.EchoblogTokenGetREQ, out *tokensproto.EchoblogTokenGetRES) error {
// 	if in.Token == "" {
// 		out.Errcode = 1
// 		return nil
// 	}
// 	tObj, err := t.M.GetToken(&models.Tokens{
// 		Token: in.Token,
// 	})
// 	if err != nil && err != sql.ErrNoRows {
// 		out.Errcode = 6
// 		return nil
// 	}
// 	if tObj.Revoke || time.Now().Before(tObj.ExpireTime) {
// 		out.Errcode = 7
// 		return nil
// 	}
// 	out.Errcode = 0
// 	out.Id = int64(tObj.ID)
// 	out.Revoke = false
// 	out.Uid = int64(tObj.UID)
// 	out.Token = tObj.Token
// 	out.Expiretime = tObj.ExpireTime.Unix()
// 	return nil
// }

// // Revoke user token
// func (t *Tokens) Revoke(ctx context.Context, in *tokensproto.EchoblogTokenRevokeREQ, out *tokensproto.EchoblogTokenRevokeRES) error {
// 	if in.Id <= 0 && in.Token == "" {
// 		out.Errcode = 1
// 		return nil
// 	}
// 	err := t.M.UpdateToken(&models.Tokens{
// 		ID:     int(in.Id),
// 		Token:  in.Token,
// 		Revoke: true,
// 	})
// 	if err != nil {
// 		out.Errcode = 8
// 		return nil
// 	}
// 	out.Errcode = 0
// 	return nil
// }
