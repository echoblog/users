package handlers

import (
	"context"
	"strings"
	"time"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/echoblog/users/srv/models"
	"gitlab.com/echoblog/utils"

	usersproto "gitlab.com/echoblog/users/srv/proto/users"
)

// UsersHandler handler
type UsersHandler struct {
	M models.UsersModel
}

// NewUsersHandler registe new grpc handler
func NewUsersHandler(m models.UsersModel) *UsersHandler {
	return &UsersHandler{
		M: m,
	}
}

// Register User
func (u *UsersHandler) Register(ctx context.Context, in *usersproto.EchoblogUsersRegisterREQ, out *usersproto.EchoblogUsersRegisterRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "UsersHandler.Register")
	defer sp.Finish()
	// logic
	if in.User.Email == "" || in.User.Password == "" {
		out.Errcode = 1
		return nil
	}
	uobj, err := u.M.GetUser(sp.Context(), &models.Users{
		Email: in.User.Email,
	})
	if err != nil {
		out.Id = 0
		out.Errcode = 3
		return nil
	}
	if uobj != nil {
		out.Id = 0
		out.Errcode = 9
		return nil
	}
	epass := utils.BcryptEncode(in.User.Password)
	id, err := u.M.InsertUser(sp.Context(), &models.Users{
		Email:      in.User.Email,
		Password:   epass,
		Sex:        uint(in.User.Sex),
		Avatar:     "",
		NickName:   strings.Split(in.User.Email, "@")[0],
		SignUpTime: time.Now(),
		LastLogin:  time.Now(),
	})
	if err != nil {
		out.Errcode = 2
		out.Id = 0
		return nil
	}
	out.Errcode = 0
	out.Id = uint64(id)
	return nil
}

// Get User details
func (u *UsersHandler) Get(ctx context.Context, in *usersproto.EchoblogUsersGetREQ, out *usersproto.EchoblogUsersGetRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "UsersHandler.Get")
	defer sp.Finish()
	// logic
	if in.Id <= 0 {
		out.Errcode = 1
		return nil
	}
	uObj, err := u.M.GetUser(sp.Context(), &models.Users{
		ID: uint64(in.Id),
	})
	if err != nil {
		out.Errcode = 3
		return nil
	}
	out.Errcode = 0
	out.Result = &usersproto.User{
		Email:         uObj.Email,
		Avatar:        uObj.Avatar,
		Id:            uint64(uObj.ID),
		Nickname:      uObj.NickName,
		Password:      uObj.Password,
		Sex:           uint64(uObj.Sex),
		Lastlogintime: uObj.LastLogin.Unix(),
		Signuptime:    uObj.LastLogin.Unix(),
	}
	return nil
}

// Update User details
func (u *UsersHandler) Update(ctx context.Context, in *usersproto.EchoblogUsersUpdateREQ, out *usersproto.EchoblogUsersUpdateRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "UsersHandler.Update")
	defer sp.Finish()
	// logic
	if in.User.Id <= 0 && in.User.Email == "" {
		out.Errcode = 1
		return nil
	}
	err := u.M.UpdateUser(sp.Context(), &models.Users{
		ID:       uint64(in.User.Id),
		Email:    in.User.Email,
		Password: utils.BcryptEncode(in.User.Password),
		NickName: in.User.Nickname,
		Avatar:   in.User.Avatar,
	})
	if err != nil {
		out.Errcode = 4
		return nil
	}
	out.Errcode = 0
	return nil
}

// Login user login logic handler
func (u *UsersHandler) Login(ctx context.Context, in *usersproto.EchoblogUsersLogineREQ, out *usersproto.EchoblogUsersLogineRES) error {
	// tracking
	sp := utils.ExtractContext(ctx, opentracing.GlobalTracer(), "UsersHandler.Login")
	defer sp.Finish()
	// logic
	if in.User.Email == "" || in.User.Password == "" {
		out.User = nil
		out.Errcode = 1
		return nil
	}
	uobj, err := u.M.GetUser(sp.Context(), &models.Users{
		Email: in.User.Email,
	})
	if err != nil {
		out.User = nil
		out.Errcode = 3
		return nil
	}
	if uobj == nil {
		out.Errcode = 10
		return nil
	}
	res := utils.BcryptCheck(in.User.Password, uobj.Password)
	if !res {
		out.User = nil
		out.Errcode = 11
		return nil
	}
	u.M.UpdateUser(sp.Context(), &models.Users{
		ID:    uint64(in.User.Id),
		Email: in.User.Email,
	})
	out.User = &usersproto.User{
		Id:            uint64(uobj.ID),
		Email:         uobj.Email,
		Avatar:        uobj.Avatar,
		Sex:           uint64(uobj.Sex),
		Nickname:      uobj.NickName,
		Signuptime:    uobj.SignUpTime.Unix(),
		Lastlogintime: uobj.LastLogin.Unix(),
	}
	out.Errcode = 0
	return nil
}
