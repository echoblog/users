package models

// import (
// 	"log"
// 	"time"
// )

// // Tokens ...
// type Tokens struct {
// 	ID         int    `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
// 	Token      string `gorm:"UNIQUE;type:varchar(72);NOT NULL"`
// 	UID        int    `gorm:"type:int(12);NOT NULL"`
// 	Revoke     bool
// 	ExpireTime time.Time
// }

// // InsertToken ...
// func (h *ModelHandler) InsertToken(t *Tokens) (err error) {
// 	d := h.db.Create(t)
// 	if err = d.Error; err != nil {
// 		log.Fatalf("users.tokens.InsertToken error[0] %s\n", err.Error())
// 		return err
// 	}
// 	return nil
// }

// // UpdateToken ...
// func (h *ModelHandler) UpdateToken(t *Tokens) (err error) {
// 	d := h.db.Where(&Tokens{ID: t.ID}).Or(&Tokens{Token: t.Token}).Update(&Tokens{Revoke: t.Revoke})
// 	if err = d.Error; err != nil {
// 		log.Fatalf("users.tokens.UpdateToken error[0] %s\n", err.Error())
// 		return err
// 	}
// 	return nil
// }

// // GetToken ...
// func (h *ModelHandler) GetToken(t *Tokens) (res *Tokens, err error) {
// 	d := h.db.Where(t).First(t)
// 	if err = d.Error; err != nil {
// 		log.Fatalf("users.tokens.GetToken error[0] %s\n", err.Error())
// 		return nil, err
// 	}
// 	return t, nil
// }
