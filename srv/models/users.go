package models

import (
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
	opentracing "github.com/opentracing/opentracing-go"
)

// Users ...
type Users struct {
	ID         uint64 `gorm:"PRIMARY_KEY;AUTO_INCREMENT"`
	Email      string `gorm:"unique_index;type:varchar(320);NOT NULL"`
	NickName   string `gorm:"type:varchar(64);NOT NULL"`
	Password   string `gorm:"type:varchar(72);NOT NULL"`
	Sex        uint   `gorm:"type:int(1);NOT NULL"`
	Avatar     string `gorm:"type:varchar(256);NOT NULL"`
	SignUpTime time.Time
	LastLogin  time.Time
}

// InsertUser ...
func (h *ModelHandler) InsertUser(ctx opentracing.SpanContext, u *Users) (id uint64, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.InsertUser", opentracing.ChildOf(ctx))
	defer ts.Finish()
	d := h.db.Create(u)
	if err = d.Error; err != nil {
		log.WithError(err).Errorln("users.users.InsertUser error[0]")
		return 0, err
	}
	return u.ID, nil
}

// UpdateUser ...
func (h *ModelHandler) UpdateUser(ctx opentracing.SpanContext, u *Users) (err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.UpdateUser", opentracing.ChildOf(ctx))
	defer ts.Finish()
	uobj := &Users{
		Password: u.Password,
		NickName: u.NickName,
		Avatar:   u.Avatar,
	}
	if !u.LastLogin.IsZero() {
		uobj.LastLogin = time.Now()
	}
	d := h.db.Model(&Users{}).Where("id = ?", u.ID).Or("email = ?", u.Email).Updates(uobj)
	if err = d.Error; err != nil {
		log.WithError(err).Errorln("users.users.UpdateUser error[0]")
		return err
	}
	return nil
}

// GetUser ...
func (h *ModelHandler) GetUser(ctx opentracing.SpanContext, u *Users) (res *Users, err error) {
	ts := opentracing.GlobalTracer().StartSpan("Models.GetUser", opentracing.ChildOf(ctx))
	defer ts.Finish()
	res = &Users{}
	d := h.db.Where(&Users{ID: u.ID}).Or(&Users{Email: u.Email}).First(res)
	err = d.Error
	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}
	if err != nil && err != gorm.ErrRecordNotFound {
		log.WithError(err).Errorln("users.users.GetUser error[0]")
		return nil, err
	}
	return res, nil
}
