package models

import (
	"github.com/jinzhu/gorm"
	"github.com/opentracing/opentracing-go"
)

// UsersModel ...
type UsersModel interface {
	// users
	InsertUser(ctx opentracing.SpanContext, u *Users) (id uint64, err error)
	UpdateUser(ctx opentracing.SpanContext, u *Users) (err error)
	GetUser(ctx opentracing.SpanContext, u *Users) (res *Users, err error)
	// tokens
	// InsertToken(t *Tokens) (err error)
	// UpdateToken(t *Tokens) (err error)
	// GetToken(t *Tokens) (res *Tokens, err error)
}

// New model
func New(db *gorm.DB) UsersModel {
	h := &ModelHandler{
		db: db,
	}

	// auto migrate tables
	h.db.AutoMigrate(&Users{})
	// h.db.AutoMigrate(&Tokens{})

	return h
}

// ModelHandler ...
type ModelHandler struct {
	db *gorm.DB
}
