package test

import (
	"context"
	"testing"

	"github.com/micro/go-micro/client"
	usersproto "gitlab.com/echoblog/users/srv/proto/users"
)

var (
	sw = true
)

func TestUsersGRPC(t *testing.T) {
	if !sw {
		return
	}
	s := usersproto.NewUsersService("go.micro.srv.echoblog.users", client.DefaultClient)
	result, err := s.Register(context.TODO(), &usersproto.EchoblogUsersRegisterREQ{
		User: &usersproto.User{
			Email:    "charlanders1@gmail.com",
			Password: "1",
			Sex:      1,
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if result.Errcode != 0 {
		t.Fatal(result)
	}
	uobj, err := s.Get(context.TODO(), &usersproto.EchoblogUsersGetREQ{
		Id:    result.Id,
		Email: "",
	})
	if err != nil {
		t.Fatal(err)
	}
	if result.Errcode != 0 {
		t.Fatal(result)
	}
	if uobj == nil {
		t.Fatal(uobj)
	}
	ures, err := s.Update(context.TODO(), &usersproto.EchoblogUsersUpdateREQ{
		User: &usersproto.User{
			Id:    uobj.Result.Id,
			Email: uobj.Result.Email,
			// Password: utils.BcryptEncode("2"),
			Avatar:   "icon",
			Nickname: "wotmshuaisi",
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if ures.Errcode != 0 {
		t.Fatal(ures)
	}
	lres, err := s.Login(context.TODO(), &usersproto.EchoblogUsersLogineREQ{
		User: &usersproto.User{
			Email:    uobj.Result.Email,
			Password: "1",
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if lres.Errcode != 0 {
		t.Fatal(lres)
	}
}
