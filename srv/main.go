package main

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/micro/go-micro/broker"

	"github.com/micro/go-plugins/broker/nats"

	"github.com/micro/go-micro/registry"

	"github.com/micro/go-plugins/registry/consul"

	"gitlab.com/echoblog/users/srv/handlers"
	"gitlab.com/echoblog/users/srv/models"
	usersproto "gitlab.com/echoblog/users/srv/proto/users"

	"github.com/jinzhu/gorm"

	ocplugin "github.com/micro/go-plugins/wrapper/trace/opentracing"
	opentracing "github.com/opentracing/opentracing-go"

	"gitlab.com/echoblog/utils"

	"github.com/micro/go-micro/server"

	"github.com/micro/go-micro"
)

func main() {
	// init logger
	initLogger()

	// database part
	db := initDB()
	defer db.Close()
	// tracer part
	t, io, err := utils.NewTracer(SERVICENAME, JAGERAGENTADDR)
	if err != nil {
		log.WithError(err).Fatalln("init tracer error occurred")
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	// service registry
	consulregistry := consul.NewRegistry(
		registry.Addrs(REGISTRYADDR),
	)

	// brokder
	natsbroker := nats.NewBroker(
		broker.Addrs(BROKERADDR),
	)

	// init service
	s := micro.NewService(
		micro.Name(SERVICENAME),
		micro.Registry(consulregistry),
		micro.Broker(natsbroker),
		micro.RegisterTTL(time.Second*30),
		micro.RegisterInterval(time.Second*15),
		micro.WrapHandler(ocplugin.NewHandlerWrapper(opentracing.GlobalTracer())),
	)

	// graceful quit
	s.Server().Init(
		server.Wait(true),
		server.Address(SERVICEADDR),
	)

	// register handlers
	usersproto.RegisterUsersServiceHandler(s.Server(), handlers.NewUsersHandler(models.New(db)))

	// run service
	if e := s.Run(); e != nil {
		log.WithError(err).Fatalln("starting service error occurred")
	}
}

// database connector
func initDB() *gorm.DB {
	connStr := SERVICEDBUSER + ":" + SERVICEDBPASS + "@tcp(" + SERVICEDBHOST +
		":" + SERVICEDBPORT + ")/" + SERVICEDBNAME + "?loc=Local&parseTime=true&charset=utf8mb4&collation=utf8mb4_unicode_ci"

	d, err := gorm.Open("mysql", connStr)
	d.DB().SetMaxIdleConns(10)
	d.DB().SetMaxOpenConns(100)
	d.DB().SetConnMaxLifetime(time.Second * 10)
	if err != nil {
		log.WithError(err).Fatalln("connect to database error occurred")
	}
	d.LogMode(false)
	return d
}

// set global logger
func initLogger() {
	logFile, err := os.OpenFile(SERVICENAME+".log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		log.SetOutput(os.Stdout)
	}
	log.SetOutput(logFile)
	log.SetFormatter(&log.JSONFormatter{})
}
