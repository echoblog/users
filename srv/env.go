package main

import "gitlab.com/echoblog/utils"

var (
	// SERVICEDBHOST database host
	SERVICEDBHOST = utils.GetEnvironment("SERVICE_DBHOST", "127.0.0.1")

	// SERVICEDBUSER database user
	SERVICEDBUSER = utils.GetEnvironment("SERVICE_DBUSER", "root")

	// SERVICEDBPASS database password
	SERVICEDBPASS = utils.GetEnvironment("SERVICE_DBPASS", "")

	// SERVICEDBPORT database port
	SERVICEDBPORT = utils.GetEnvironment("SERVICE_DBPORT", "3306")

	// SERVICEDBNAME database name
	SERVICEDBNAME = utils.GetEnvironment("SERVICE_DBNAME", "echoblog_users")

	// SERVICENAME microservice name
	SERVICENAME = utils.GetEnvironment("SERVICE_NAME", "go.micro.srv.echoblog.users")

	// SERVICEADDR microservice listen address
	SERVICEADDR = utils.GetEnvironment("SERVICE_ADDR", "")

	// JAGERAGENTADDR jaeger agent address
	JAGERAGENTADDR = utils.GetEnvironment("JAEGER_AGENT_ADDR", "127.0.0.1:6831")

	// BROKERADDR registry address
	BROKERADDR = utils.GetEnvironment("BROKER_ADDR", "127.0.0.1:4222")

	// REGISTRYADDR registry address
	REGISTRYADDR = utils.GetEnvironment("REGISTRY_ADDR", "127.0.0.1:8500")
)
